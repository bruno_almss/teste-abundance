import "./style.scss";
import Button from "../Button";


const ValueCompra = ( ) => {
   
    return(
        <div className="total-compra">
          <p className="subheading text-semibold text-dark-green">Total: <span className="text-purple">R$ 0</span></p>
          <Button className="button-comprar" disabled >Comprar</Button>
        </div>
    );
  }
  export default ValueCompra
  

