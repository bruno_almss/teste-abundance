import React, { useEffect, useState } from "react";
import "./style.scss";
import Button from "../../components/Button";
import { useHistory } from "react-router-dom";
import Select from "../../components/Select";
import Drawer from "../../components/SliderModal";
import FormPerguntasFrequentes from "../../containers/DrawerInformations/FormPerguntasFrequentes";
import TitleSubtitle from "../../components/Titles/TitleSubtitle";
import Input from "../../components/Input";
import Textarea from "../../components/Textarea";
import TitleHeaderActionIcon from "../../components/Titles/TitleHeaderActionIcon";

const SuporteChamado = () => {
  const [openDrawerPerguntas, setOpenDrawerPerguntas] = useState(false);
  const [openChamado, setOpenChamado] = useState(false);

  const history = useHistory();

  const goToSuport = () => {
    history.push("/suporte");
  };

  const handleOpenDrawerPerguntas = () => {
    setOpenDrawerPerguntas(true);
  };

  const handleOpenChamado = () => {
    setOpenChamado(true);
    if (openChamado) setOpenChamado(false);
  };

  const handleSubmitSupport = () => {
    setOpenChamado(false);
    alert("Ainda não implementado, mas submetido!");
  };

  useEffect(() => {
    if (openDrawerPerguntas) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "unset";
    }
  }, [openDrawerPerguntas]);

  return (
    <div className="container-fluid">
      <div className="confirmation-content container-fluid">
        <TitleHeaderActionIcon title="SUPORTE"/>
        <div className="chamados">
          <TitleSubtitle title="Chamados" subtitle="Você possui 2 chamados" />

          <div className="botaochamado">
            <Button type="submit" className="sm" onClick={handleOpenChamado}>
              {!openChamado ? "Solicitar novo chamado" : "Cancelar"}
            </Button>
          </div>
        </div>
        {openChamado ? (
          <>
            <div>
              <h1>Novo chamado</h1>
            </div>
            <div className="row">
              <div className="md-12">
                <Input
                  name="nome"
                  type="text"
                  label="Título"
                  placeholder="Título do chamado"
                />
                <Select name="ondeOuviu" label="Assunto">
                  <option value="google">Reclamação</option>
                </Select>
                <span className="label mb-2">Mensagem</span>
                <Textarea
                id="textarea-suporte"
                className="textarea-suporte"
                type="text"
                ></Textarea>
                

                <p>
                  Consulte nossas{" "}
                  <Button
                    type="button"
                    className="link button-alinhado"
                    onClick={handleOpenDrawerPerguntas}
                  >
                    Perguntas frequentes{" "}
                  </Button>{" "}
                  ou entre em contato com o suporte{" "}
                  <Button
                    type="button"
                    className="link button-alinhado"
                    onClick={goToSuport}
                  >
                    aqui
                  </Button>
                  .
                </p>
                <Button
                  type="submit"
                  className="md tamanhobotao mt-6 f-right"
                  onClick={handleSubmitSupport}
                >
                  Enviar
                </Button>
              </div>


            </div>
          </>
        ) : (
          ""
        )}

        <div className="container-table mt-9">
          <table>
            <tr className="text-lightGray">
              <th>ASSUNTO</th>
              <th>REFERÊNCIA</th>
              <th>DATA</th>
              <th>STATUS</th>
            </tr>
            <tr className="text-dark-green">
              <td>
                <b>RECLAMAÇÃO</b>
              </td>
              <td>#01234554454</td>
              <td>8 de Março de 2021</td>
              <td className="text-red text-bold">ABERTO</td>
            </tr>
            <tr className="text-dark-green ">
              <td>
                <b>RECLAMAÇÃO</b>
              </td>
              <td>#01234554455</td>
              <td>8 de Março de 2021</td>
              <td className="text-green text-bold">CONCLUÍDO</td>
            </tr>
          </table>
        </div>
      </div>
      <Drawer
        opened={openDrawerPerguntas}
        onCLose={() => setOpenDrawerPerguntas(false)}
        title="Perguntas Frequentes"
      >
        <FormPerguntasFrequentes />
      </Drawer>
    </div>
  );
};
export default SuporteChamado;
