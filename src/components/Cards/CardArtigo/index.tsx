import React from "react";
import { useState } from "react";
import "./style.scss";
import Mask from "../../../assets/img/Mask.png";

export default function CardArtigo() {
  const [counter, setCounter] = useState(0);

  
  return (
    <div className="card-artigo">
      <div className="title-qtd ">
        <img src={Mask}/>
        <h5 className="heading-06">Lorem ipsum dolor sit</h5>
        <p>Sagittis vitae et leo duis ut diam quam malesuada fames ac turpis.</p>
      </div>
    </div>
  );
}
