import Button from "../../../components/Button";
import "./style.scss";
import Verdeazul from "../../../assets/img/Verdeazul.png";
import Video from "../../../assets/img/VideoVA.png";
import Gallery from "../../../assets/img/GalleryVA.png";
import Satellite from "../../../assets/img/Satellite.png";
import Milestones from "../../../assets/img/Milestones.png";
import Linhatempo from "../../../assets/img/Linhatempo.png";
import TabsVA from "../../../components/AllTabes/TabsVA";
import Oval from "../../../assets/img/Oval.png";

const FormVerdeAzul = () => {
  return (
    <div className="align-modal modalverdeazul">
      <img src={Verdeazul} className="width-modal" />
      <h2>VERDEAZUL</h2>
      <p>
        <b>
          O Verdeazul é um case de sucesso com o propósito de esverdear o Brasil
          através do reflorestamento.
        </b>
        <br></br> Contribua com a melhoria do meio ambiente: invista em tokens
        que correspondem a uma árvore e acompanhe o desenvolvimento dela..
      </p>
      <iframe
        width="100%"
        height="420px"
        src="https://www.youtube.com/embed/_2c8YZwzaMk"
        title="YouTube video player"
        frameBorder={0}
        style={{ border: 0, borderRadius: "12px" }}
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
      <h3 className="margin-va">1 milhão de árvores</h3>
      <p>
        Comprometimento rumo à elevar e acelerar o que o Brasil tem de melhor:
        sustentabilidade e abundância ambiental.
        <br></br>
        <br></br>Uma árvore, em média, sequestra 1 tonelada de C02 no seu ciclo
        de vida de 40 anos. Assim, Abundance Tokens foram emitidos para
        preservar cada uma dessas árvores. Invista no reflorestamento e na
        preservação do meio ambiente ao adquirir o seu token e conseguir
        acompanhar o desenvolvimento da sua árvore.
      </p>
      <img src={Gallery} className="width-modal" />
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d29125.698532084825!2d-46.730955050000006!3d-24.146739200000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1643802521752!5m2!1spt-BR!2sbr"
        width="100%"
        height="420"
        style={{ border: 0, borderRadius: "12px" }}
        allowFullScreen={false}
        loading="lazy"
        className="mt-4"
      ></iframe>
      <img src={Milestones} className="width-modal margin-va" />
      <img src={Linhatempo} className="width-modal margin-va" />
      <div className="div-card-va margin-va">
        <h2>Atuação ESG</h2>
        <TabsVA />
      </div>
      <div className="div-card-va margin-va margin-bt-page">
        <h2>Consultor responsável</h2>
        <div className="div-flex divprofile">
          <img src={Oval} />
          <div className="title-profile">
            <h4 className="p-margin">Anderson Souza</h4>
            <p className="p-margin">Business & Development</p>
          </div>
        </div>
        <div className="div-flex margin-va">
          <span className="material-icons color-icon">alternate_email</span>
          <p className="p-margin">anderson@abundancebrasil.com</p>
        </div>
        <div className="div-flex space-info">
          <span className="material-icons color-icon">whatsapp</span>
          <p className="p-margin">+55 (31) 9 9175-7021</p>
        </div>
      </div>
    </div>
  );
};

export default FormVerdeAzul;
