import React from "react";
import "./style.scss";
import Perfil from "../../assets/img/Avatars.png";
import SearchMain from "../SearchMain";
import LogoWhite from "../../assets/img/Logo-white-mobile.svg";
import SearchLanguage from "../SearchLanguage";
import { useDispatch, useSelector } from "react-redux";
import BandeiraBrasil from "../../assets/img/brasil-bandeira.svg";


const MainHeader = () => {
  const dispatch = useDispatch();
  const dataUser = useSelector((state: any) => state.user.data)

  const changeMenu = () => {
    dispatch({ type: "MENU_TOGGLE"});
  }

  const changeMenuProfile = () => {
    dispatch({ type: "MENU_TOGGLE_PROFILE"});
  }

  

  return (
    <header className="text-lightGray _interno">
      <SearchMain className="hidden-mobile" />
      <div className="buttonLogoHeader alinhar">
        <button style={{cursor: "pointer"}} onClick={() => changeMenu()} className="material-icons">
          menu
        </button>

        <div className="logo-mobile">
          <img src={LogoWhite} alt="" />
        </div>
      </div>
      <div className="InfoHeader alinhar">
        <SearchLanguage img={BandeiraBrasil} />

        <a href="#">
          <span className="material-icons text-lightGray alinhar mr-4-5">
            help_outline
          </span>
        </a>

        <span className="material-icons mr-4-5">notifications</span>

        <div className="perfilHeader alinhar">
          {dataUser && <p>Olá, {dataUser.name}!</p>}
          <img src={Perfil} alt="" style={{cursor: "pointer"}} onClick={() => changeMenuProfile()} />
        </div>
      </div>
    </header>
  );
};
export default MainHeader;
