import CardArtigo from "../../components/Cards/CardArtigo";
import FormAlterarSenha from "../../containers/FormAlterarSenha";
import "./style.scss";
import Floresta from "../../assets/img/Floresta.png";
import TitleHeaderActionIcon from "../../components/Titles/TitleHeaderActionIcon";
import TitleSubtitle from "../../components/Titles/TitleSubtitle";
import Button from "../../components/Button";

const SuporteInt = () => {
  return (
    <div className="mt-20 container-artint container-fluid content-side _interno fluid-header-actions">
      <div className="row">
        <div className="xs-12 mb-24">
          <TitleHeaderActionIcon title="Suporte" />
          <div className="title-cham">
            <TitleSubtitle title="Chamados" />
          </div>
          <div className="title-cham">
            <h2>Assunto</h2>
            <p>Reclamação</p>
          </div>
          <div className="title-cham">
            <h2>Status</h2>
            <p className="txt-red">ABERTO</p>
          </div>
          <div className="title-cham">
            <h2>Mensagem</h2>
            <p className="txt-area">Non odio euismod lacinia at quis risus sed. Etiam sit amet nisl purus in mollis nunc sed. Et malesuada fames ac turpis. Nisl nunc mi ipsum
              <br></br><br></br>
              faucibus vitae aliquet nec ullamcorper sit. Turpis egestas sed tempus urna et pharetra pharetra. Sit amet venenatis urna cursus eget.
              <br></br><br></br>
              Ullamcorper eget nulla facilisi etiam dignissim diam quis enim. Habitasse platea dictumst vestibulum rhoncus est. Faucibus ornare suspendisse sed nisi lacus sed viverra. Morbi quis commodo odio aenean. Diam sollicitudin tempor id eu nisl nunc. Tortor vitae purus faucibus ornare suspendisse. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor elit. Mi bibendum neque egestas congue quisque egestas diam in arcu. Ut eu sem integer vitae justo eget magna. Viverra ipsum nunc aliquet bibendum enim facilisis gravida neque convallis. Diam in arcu cursus euismod quis viverra nibh.</p>
          </div>
          <div className="colun-btn">
            <div className="btn-return">
              <span className="material-icons color-icon">arrow_back</span>
              <h4 className="espaco-l">Retornar para lista de artigos</h4>
            </div>
            <div className="btn-return">
              <h4 className="espaco-r">Remover chamado</h4>
              <span className="material-icons color-icon">delete</span>
            </div>
            <Button type="submit" className="sm btn-left">
              Finalizar chamado
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SuporteInt;
