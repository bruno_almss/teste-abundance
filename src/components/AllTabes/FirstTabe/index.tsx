import React from "react";
import PhotoTab from "../../../assets/img/photoTab.png"
import "./style.scss"

interface PropsButton {
  className?: string;
  link?: string;
}


export default function Button(props: PropsButton) {
  return (
    <div className={` ${props.className}`} >
      
      <img src={PhotoTab} alt="" />
    <div className="contentTab">
      <p className="caption mb-1 text-lightGray ">verdeazul</p>
      <h5 className="text-normal heading-05 text-dark-green mb-1">Projeto 1 milhão de árvores</h5>
      <p className="mb-1 text-lightGray ">Invista no reflorestamento e na preservação do meio ambiente ao 
        adquirir o seu token e conseguir acompanhar o desenvolvimento da sua árvore.
      </p>
      <a className="p-01 text-medium-green" href="#">Saiba mais</a>
    </div>


    </div>
  );
};
