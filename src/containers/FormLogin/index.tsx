import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import * as Yup from "yup";

import Button from "../../components/Button";
import Input from "../../components/Input";
import "./style.scss";
import { yupResolver } from "@hookform/resolvers/yup";

import FormLoginData from "../../types/FormLogin.interface";
import LoginService from "../../services/requests/login";
import LinkButton from "../../components/LinkButton";

const FormLogin = () => {
  const [valueForm, setValueForm] = useState({});
  const history = useHistory();

  const validationSchema = Yup.object().shape({
    login: Yup.string().required("Login é obrigatório"),
    password: Yup.string().required("Senha é obrigatório"),
  });

  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<FormLoginData>({
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = (data: FormLoginData) => {
    LoginService.login(data)
      .then(() => {
        return history.push("/confirmacao");
      })
      .catch(() => {
        return history.push("/confirmacao");
      });
  };

  const changeForm = (target: HTMLInputElement) => {
    setValueForm({ ...getValues(), [target.name]: target.value });
  };

  return (
    <form className="align-center-form" onSubmit={handleSubmit(onSubmit)}>
      <h2>Acesse sua conta</h2>
      <p>Digite seus detalhes abaixo</p>

      <Input
        register={register("login")}
        error={errors.login?.message}
        name="login"
        type="text"
        label="Login"
        placeholder="meuLogin"
        onChange={(e) => changeForm(e.target)}
      />
      <LinkButton to="/esqueci-minha-senha" className="link text-right w-fill p-relative" style={{marginBottom: "-20px"}}>Esqueci minha senha</LinkButton>
      <Input
        register={register("password")}
        error={errors.password?.message}
        name="password"
        label="Senha"
        type="password"
        placeholder="•••••••••••••••••••••••"
        onChange={(e) => changeForm(e.target)}
      />

      <Button
        type="submit"
        className="md"
        disabled={!validationSchema.isValidSync(valueForm)}
      >
        Entrar
      </Button>
    </form>
  );
};

export default FormLogin;
